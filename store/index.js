export const state = () => ({
  route: {
    course:null,
    chapter:null,
    lesson:null,
  },
  drawer: true
})

export const mutations = {
  setCourse(state, course) {
    state.route.course = course
  },
  setChapter(state, chapter) {
    state.route.chapter = chapter
  },
  setLesson(state, lesson) {
    state.route.lesson = lesson
  },
  setDrawer(state, drawer) {
    state.drawer = drawer;
  },
  toggleDrawer(state) {
    state.drawer = !state.drawer
  }
}

export const getters = {
  breadcrumbs: ({ route }) => {
    let breadcrumbs = [];
    const {course, chapter, lesson} = route;
    breadcrumbs.push({
      text: course.displayName,
      disabled: false,
      href: `/courses/${course.id}`
    });
    if (chapter){
      breadcrumbs.push({
        text: chapter.displayName,
        disabled: false,
        href: `/courses/${course.id}/${chapter.id}`
      });
      if (lesson){
        breadcrumbs.push({
          text: lesson.displayName,
          disabled: true,
          href: `/courses/${course.id}/${chapter.id}/${lesson.id}`
        })
      }
    }
    return breadcrumbs;
  }
}
