/**
 * Interface for a Subject. Subjects are containers for chapters.
 * A subject is equivalent to a class, such as Intermediate Algebra.
 *
 * @interface
 */
export interface ISubject {
  /**
   * The name of the Subject, used for displaying in applications
   */
  displayName: string;
  /**
   * The description of the Subject, provides information about what will be
   * covered and who should take the course
   */
  description: string;
  /**
   * The unique identifier of a subject. Has 10 characters, is a number.
   */
  id: string;
  /**
   * @deprecated Use `id` instead.
   */
  pathname: string;
  /**
   * An array of Chapter IDs, which correspond to chapters that the Subject contains.
   */
  chapters: string[];
  /**
   * An array of Subject IDs, which are recommended to take before this Subject.
   */
  prerequisites: string[],
  /** The Category ID that this subject is associated with */
  category: string;

}

/** Interface for Chapter. A Chapter is a part of a subject, such as Conics within Algebra. */
export interface IChapter {
  /**
   * The name of the Chapter, used for displaying in applications
   */
  displayName: string;
  /**
   * The description of the Chapter, provides information about what will be
   * covered
   */
  description: string;
  /**
   * The unique identifier of a chapter. Has 10 characters, is a number.
   */
  id: string;
  /**
   * @deprecated Use `id` instead.
   */
  pathname: string;
  /**
   * An array of Lesson IDs, which correspond to chapters that the Subject contains.
   */
  lessons: string[];
  /** The Subject ID that this chapter is associated with */
  subject: string;
  /** The Category ID that this chapter is associated with */
  category: string;
}


/** Interface for Lesson. A Lesson is a part of a Chapter, such as Hyperbolas within Conics. */
export interface ILesson {
  /**
   * The name of the Lesson, used for displaying in applications
   */
  displayName: string;
  /**
   * The description of the Lesson, provides information about what will be
   * covered
   */
  description: string;
  /**
   * The unique identifier of a lesson. Is the same as the pathname.
   */
  id: string;
  /**
   * @deprecated Use `id` instead.
   */
  pathname: string;
  /** The Subject ID that this lesson is associated with */
  subject: string;
  /** The Chapter ID that this lesson is associated with */
  chapter: string;
  /** The Category ID that this lesson is associated with */
  category: string;
}
