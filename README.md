# YMath.io

This is the source code for the site redesign of https://ymath.io/. 

## Writing Content

Course content is structured in three tiers: courses, chapters, and lessons.

Courses are the equivalent of an academic course one might take on AoPS, such as Intermediate Algebra. 
Chapters are specific topics, such as Conics.
Lessons are subtopics, such as hyperbolas. Lessons should be short enough to be completed in 10-15 minutes.

See existing course content to understand formatting. 

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
